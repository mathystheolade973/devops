-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.4.24-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour cuisine
CREATE DATABASE IF NOT EXISTS `cuisine` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `cuisine`;

-- Listage de la structure de la table cuisine. composerrecette
CREATE TABLE IF NOT EXISTS `composerrecette` (
  `id_compo` int(11) NOT NULL AUTO_INCREMENT,
  `id_ingredient` int(11) NOT NULL DEFAULT 0,
  `id_recette` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_compo`),
  KEY `id_ingredient` (`id_ingredient`),
  KEY `id_recette` (`id_recette`),
  CONSTRAINT `id_ingredient` FOREIGN KEY (`id_ingredient`) REFERENCES `ingredients` (`id_ingredient`),
  CONSTRAINT `id_recette` FOREIGN KEY (`id_recette`) REFERENCES `recette` (`id_recette`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table cuisine.composerrecette : ~11 rows (environ)
/*!40000 ALTER TABLE `composerrecette` DISABLE KEYS */;
INSERT INTO `composerrecette` (`id_compo`, `id_ingredient`, `id_recette`) VALUES
	(1, 1, 1),
	(2, 6, 1),
	(3, 2, 1),
	(4, 1, 3),
	(5, 8, 3),
	(6, 4, 3),
	(7, 2, 3),
	(8, 9, 2),
	(9, 1, 2),
	(10, 2, 2),
	(12, 3, 2);
/*!40000 ALTER TABLE `composerrecette` ENABLE KEYS */;

-- Listage de la structure de la table cuisine. ingredients
CREATE TABLE IF NOT EXISTS `ingredients` (
  `id_ingredient` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ingredient`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table cuisine.ingredients : ~13 rows (environ)
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` (`id_ingredient`, `nom`) VALUES
	(1, 'farine'),
	(2, 'oeuf'),
	(3, 'sucre'),
	(4, 'lait'),
	(5, 'vanille'),
	(6, 'pomme'),
	(7, 'poire'),
	(8, 'caramel'),
	(9, 'fraise'),
	(10, 'chocolat'),
	(11, 'orange'),
	(12, 'poudre amande'),
	(13, 'noisette');
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;

-- Listage de la structure de la table cuisine. recette
CREATE TABLE IF NOT EXISTS `recette` (
  `id_recette` int(11) NOT NULL AUTO_INCREMENT,
  `nom_recette` varchar(50) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id_recette`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table cuisine.recette : ~3 rows (environ)
/*!40000 ALTER TABLE `recette` DISABLE KEYS */;
INSERT INTO `recette` (`id_recette`, `nom_recette`, `description`) VALUES
	(1, 'tarte aux pommes', 'Delicieuse tarte aux pommes facile et rapide'),
	(2, 'tarte aux fraise', 'Délicieuse tarte aux fraise pour l\'été'),
	(3, 'tarte aux poires', 'Délicieuse tarte aux poires de grand-mère');
/*!40000 ALTER TABLE `recette` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
