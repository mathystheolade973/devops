/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.o7planning.test1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author pelod
 */
public class calculatriceTest {

    public calculatriceTest() {
    }

    @org.junit.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.junit.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }

    @org.junit.Test
    public void testLireRecette() throws SQLException {
        System.out.println("testLireRecette");
        ConnecteurDB monCon = new ConnecteurDB("db", "3306", "cuisine", "root", "example");
        boolean monBool = monCon.LireRecette("pomme");
        monCon.Disconnect();
        assertEquals(true, monBool);
    }

    @org.junit.Test
    public void testCreerRecette() throws SQLException {
        System.out.println("testCreerRecette");
        ConnecteurDB monCon = new ConnecteurDB("db", "3306", "cuisine", "root", "example");
        boolean monBool = monCon.CreerRecette("Tarte aux peches", "Une magnifique tarte aux peches");
        monCon.Disconnect();
        assertEquals(true, monBool);
    }

    @org.junit.Test
    public void testMajRecette() throws SQLException {
        System.out.println("testMajRecette");
        ConnecteurDB monCon = new ConnecteurDB("db", "3306", "cuisine", "root", "example");
        boolean monBool = monCon.MajRecette("peches", "Une plus si belle tarte aux peches");
        monCon.Disconnect();
        assertEquals(true, monBool);
    }

    @org.junit.Test
    public void testAjoutIngredientRecette() throws SQLException {
        System.out.println("testAjoutIngredientRecette");
        ConnecteurDB monCon = new ConnecteurDB("db", "3306", "cuisine", "root", "example");
        boolean monBool = monCon.AjoutIngredientRecette(monCon.RecupererDerniereRecette(), "pomme");
        monBool = monBool && monCon.SuprIngredientRecette(monCon.RecupererDerniereRecette(), "pomme");
        monBool = monBool && monCon.DeleteRecette((monCon.RecupererDerniereRecette()));
        monCon.Disconnect();
        assertEquals(true, monBool);
    }
}
