package org.o7planning.test1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 33695
 */
public class ConnecteurDB {

    Connection db;
    Map<String, String> connectedUser = new HashMap<>();

    /*
    * Constructeur de la classe DatabaseManager
    * Initialisation de la connexion à la bdd
    * créé un obejet db
     */
    public ConnecteurDB(String connexionString, String port, String database, String user, String password) throws SQLException,  {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String uri = "jdbc:mysql://" + connexionString  + ":" + port  +"/" + database;
            System.out.println(uri);
            this.db = DriverManager.getConnection(uri, user, password);
        } catch (SQLException e) {
            System.out.print(e);
            System.out.println("Echec de la connexion à la base de données:");
            System.exit(1);
        }
    }

    /*
    * Fermeture de la connexion à la BDD
     */
    public void Disconnect() throws SQLException {
        this.db.close();
    }

    public boolean CreerRecette(String nom, String description) throws SQLException {
        try {
            String sql = "Insert into recette (nom_Recette, description) values('" + nom + "','" + description + "');";
            PreparedStatement preparedStatement = this.db.prepareStatement(sql);
            preparedStatement.executeUpdate();
            System.out.println("Insertion réussie");
            return true;
        } catch (SQLException e) {
            System.out.println("Echec de l'insertion.");
            System.out.println(e);
            return false;
        }
    }

    public int RecupererDerniereRecette() throws SQLException {
        try {
            String sql = "select max(id_recette)as max from recette ";
            PreparedStatement preparedStatement = this.db.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();
            result.first();
            return result.getInt("max");
        } catch (SQLException e) {
            System.out.println("Echec de l'exécution de la requête.");
            System.out.println(e);
            return 0;
        }
    }

    public boolean LireRecette(String motPresent) throws SQLException {
        try {
            String sql = "SELECT * from recette";
            PreparedStatement preparedStatement = this.db.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                if (result.getString("nom_Recette").contains(motPresent)) {
                    this.connectedUser.put("id_Recette", String.valueOf(result.getInt("id_Recette")));
                    this.connectedUser.put("nom_Recette", result.getString("nom_Recette"));
                    this.connectedUser.put("description", result.getString("description"));
                    System.out.println(this.connectedUser.toString());
                }
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Echec de l'exécution de la requête.");

            System.out.println(e);
            return false;
        }
    }

    public boolean MajRecette(String nomRecette, String description) throws SQLException {
        try {
            String sql = "SELECT * from recette";
            PreparedStatement preparedStatement = this.db.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                if (result.getString("nom_Recette").contains(nomRecette)) {
                    description = description.replaceAll("'", "");
                    String sql2 = "UPDATE recette set description = '" + description + "' where id_Recette = " + result.getInt("id_Recette");
                    PreparedStatement preparedStatement2 = this.db.prepareStatement(sql2);
                    preparedStatement2.executeUpdate();
                    System.out.println("Modification réussie");

                    this.connectedUser.put("id_Recette", String.valueOf(result.getInt("id_Recette")));
                    this.connectedUser.put("nom_Recette", result.getString("nom_Recette"));
                    this.connectedUser.put("description", result.getString("description"));
                    System.out.println(this.connectedUser.toString());
                }
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Echec de l'exécution de la requête");
            System.out.println(e);
            return false;
        }
    }

    public boolean DeleteRecette(int id) throws SQLException {
        try {
            String sql = "SELECT * from recette";
            PreparedStatement preparedStatement = this.db.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                if (result.getInt("id_Recette") == id) {

                    String sql2 = "DELETE from composerrecette where id_Recette = " + result.getInt("id_Recette");
                    PreparedStatement preparedStatement2 = this.db.prepareStatement(sql2);
                    preparedStatement2.executeUpdate();

                    String sql3 = "DELETE from recette where id_Recette = " + result.getInt("id_Recette");
                    PreparedStatement preparedStatement3 = this.db.prepareStatement(sql3);
                    preparedStatement3.executeUpdate();
                    System.out.println("Supression réussie");

                    this.connectedUser.put("id_Recette", String.valueOf(result.getInt("id_Recette")));
                    this.connectedUser.put("nom_Recette", result.getString("nom_Recette"));
                    this.connectedUser.put("description", result.getString("description"));
                    System.out.println(this.connectedUser.toString());
                }
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Echec de l'exécution de la requête.");
            System.out.println(e);
            return false;
        }
    }

    public boolean AjoutIngredientRecette(int idRecette, String strIngredient) throws SQLException {
        try {
            String sql = "SELECT * from ingredients";
            PreparedStatement preparedStatement = this.db.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                if (result.getString("nom").contains(strIngredient)) {

                    String sql3 = "INSERT INTO composerrecette (id_ingredient, id_recette) VALUES (" + result.getInt("id_ingredient") + ", " + idRecette + ")";
                    PreparedStatement preparedStatement3 = this.db.prepareStatement(sql3);
                    preparedStatement3.executeUpdate();
                    System.out.println("Insertion réussie");
                }
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Echec de l'exécution de la requête.");
            System.out.println(e);
            return false;
        }
    }

    public boolean SuprIngredientRecette(int idRecette, String strIngredient) throws SQLException {
        try {
            String sql = "SELECT * from ingredients";
            PreparedStatement preparedStatement = this.db.prepareStatement(sql);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                if (result.getString("nom").contains(strIngredient)) {

                    String sql3 = "DELETE from composerrecette where id_Recette = " + idRecette + " and id_ingredient = " + result.getInt("id_ingredient");
                    PreparedStatement preparedStatement3 = this.db.prepareStatement(sql3);
                    preparedStatement3.executeUpdate();
                    System.out.println("Supression réussie");
                }
            }
            return true;
        } catch (SQLException e) {
            System.out.println("Echec de l'exécution de la requête.");
            System.out.println(e);
            return false;
        }
    }
}
