/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.o7planning.test1;

import java.sql.SQLException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 *
 * @author pelod
 */
public class main {

    public static void main(String[] args) throws IOException {
        Boolean Sortie = true;
        int choix;
        BufferedReader br2;
        String str2;
        BufferedReader br3;
        String str3;

        try {

            ConnecteurDB monCon = new ConnecteurDB("db", "3306", "cuisine", "root", "example");

            System.out.println(monCon.RecupererDerniereRecette());
            while (Sortie) {
                System.out.println("---------------------------------------------");

                System.out.println("Choisir une action : ");
                System.out.println("1 : Creer une recette");
                System.out.println("2 : Lire une recette");
                System.out.println("3 : Mettre à jour une recette");
                System.out.println("4 : Supprimer une recette");
                System.out.println("5 : Ajouter un ingredient à une recette");
                System.out.println("6 : Supprimer un ingrédient d'une recette");
                System.out.println("7 : Sortir");
                System.out.println("---------------------------------------------");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String str = br.readLine();
                //Regex pour vérifier que l'input de l'user est bien un entier 
                if (str.matches("[+-]?\\d*(\\.\\d+)?")) {
                    choix = Integer.parseInt(str);
                } else {
                    choix = 0;
                }
                switch (choix) {
                    case 1:
                        System.out.println("Veuillez choisir le nom de la recette : ");
                        br2 = new BufferedReader(new InputStreamReader(System.in));
                        str2 = br2.readLine();
                        System.out.println("Veuillez ajouter une description à la recette : ");
                        br3 = new BufferedReader(new InputStreamReader(System.in));
                        str3 = br3.readLine();
                        monCon.CreerRecette(str2, str3);
                        break;

                    case 2:
                        System.out.println("Veuillez choisir une recette dans le catalogue : ");
                        br2 = new BufferedReader(new InputStreamReader(System.in));
                        str2 = br2.readLine();

                        monCon.LireRecette(str2);
                        break;

                    case 3:
                        System.out.println("Veuillez indiquer la recette à mette à jour : ");
                        br2 = new BufferedReader(new InputStreamReader(System.in));
                        str2 = br2.readLine();
                        System.out.println("Veuillez indiquer la description de la recette à mette à jour : ");
                        br3 = new BufferedReader(new InputStreamReader(System.in));
                        str3 = br3.readLine();
                        monCon.MajRecette(str2, str3);
                        break;

                    case 4:
                        System.out.println("Veuillez indiquier l'identifiant de la recette à supprimer : ");
                        br2 = new BufferedReader(new InputStreamReader(System.in));
                        str2 = br2.readLine();
                        if (str2.matches("[+-]?\\d*(\\.\\d+)?")) {
                            monCon.DeleteRecette(Integer.parseInt(str2));
                        } else {
                            System.out.println("Ceci n'est pas un identifiant");
                        }
                        break;

                    case 5:
                        System.out.println("Veuillez indiquier l'identifiant de la recette dans laquelle ajouter l'ingrédient : ");
                        br2 = new BufferedReader(new InputStreamReader(System.in));
                        str2 = br2.readLine();
                        System.out.println("Veuillez saisir le nom de l'ingrédient à ajouter : ");
                        br3 = new BufferedReader(new InputStreamReader(System.in));
                        str3 = br3.readLine();
                        if (str2.matches("[+-]?\\d*(\\.\\d+)?")) {
                            monCon.AjoutIngredientRecette(Integer.parseInt(str2), str3);
                        } else {
                            System.out.println("Ceci n'est pas un identifiant");
                        }
                        break;

                    case 6:
                        System.out.println("Veuillez indiquier l'identifiant de la recette dans laquelle supprimer l'ingrédient : ");
                        br2 = new BufferedReader(new InputStreamReader(System.in));
                        str2 = br2.readLine();
                        System.out.println("Veuillez sasir le nom de l'ingrédient à supprimer : ");
                        br3 = new BufferedReader(new InputStreamReader(System.in));
                        str3 = br3.readLine();
                        if (str2.matches("[+-]?\\d*(\\.\\d+)?")) {
                            monCon.SuprIngredientRecette(Integer.parseInt(str2), str3);
                        } else {
                            System.out.println("Ceci n'est pas un identifiant");
                        }
                        break;

                    case 7:
                        Sortie = false;
                        break;

                    default:
                        System.out.println("Choix incorrect");
                        break;
                }
            }
            monCon.Disconnect();
        } catch (SQLException e) {
            System.out.println(e);
        }
        System.out.println("Merci de votre visite.");
    }

}
