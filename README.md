# Groupes





### Groupe 1



Theolade Mathys, Hoarau Noah, Jenvrain Lucas, Rassant Nolan





### Groupe 2



Valentin Marie, Parsy Elodie, Maxant Alexandre, Victor Perreira





### Pourquoi



Nous avons décidé de faire une mise en commun au sein de ce dépôt gitlab, car nous avons rencontré exactement les mêmes problèmes alors que nous avons démarré séparément.



Nous avons décidé d'être honnêtes et de vous partager un seul répot au lieu de deux en modifiant des variables...





# Projet



Nous avons développé un projet en java réalisant toutes les **Users Stories** demandées **(CRUD)**







#### Besoin 1 :



*En tant qu’utilisateur, je dois pouvoir créer une recette (nom,



description)*



#### Besoin 2 :



*En tant qu’utilisateur, je dois pouvoir lire une ou plusieurs recettes (id,



nom, description et ingrédients de la recette) à partir d’un mot composant le nom



de la recette et saisi par l’utilisateur.*



#### Besoin 3 :



*En tant qu’utilisateur, je dois pouvoir mettre à jour une recette (nom,



description ou ingrédients de la recette)*



#### Besoin 4 :



*En tant qu’utilisateur, je dois pouvoir supprimer une recette (à partir de



l’id saisi par l’utilisateur)*



#### Besoin 5 :



*En tant qu’utilisateur, je dois pouvoir ajouter un ingrédient à une recette



(définir par son id)*



#### Besoin 6 :



*En tant qu’utilisateur, je dois pouvoir supprimer un ingrédient d’une



recette (définir par son id)*





# Docker



Une fois cela effectué, nous avons mis en place le **docker** en utilisant `docker compose`.



Ce docker compose crée trois containers:





#### DB :



Ce container lance la base de données, une fois la base de données mise en place, il va automatiquement aller récupérer le script qui va importer la base de données le fichier étant dans `docker/mysql-dump/cuisine/DB_Cuisine.sql`. De plus, nous avons mis en place un système de **healthcheck** ce qui veut dire que les autres containers ne se lanceront pas tant que la base n'est pas instanciée.





#### Java :



Ce container contient l'application **Java** et a pour but de compiler l'application maven





#### Test :



Ce container lance les **tests** de l'application maven







#### Connexion BDD



Pour la connexion à la bdd, nous avons utilisé cette chaîne de connexion :



`jdbc:mysql://db:3306/cuisine`





# CI / CD





Nous avons setup un **Runner** en local afin de pouvoir lancer les pipelines automatiquement lors d'un push effectué sur gitlab.



Nous avons aussi mis en place la configuration pipeline dans le fichier `gitlab-ci.yml` cependant, il n'a pas eu le comportement désiré malgré nos nombreuses tentatives.
